#include <QTableView>

#include "CommonTypes.h"
#include "CalendarManager.h"

const QString       CalendarManager::m_bgDayOff("#c0c0ff");
const QString       CalendarManager::m_fgDayOff("black");
const QString       CalendarManager::m_bgWorkDay("white");
const QString       CalendarManager::m_fgWorkDay("black");

CalendarManager::CalendarManager(QWidget *parent)
    : QCalendarWidget(parent)
{
    m_workdayFormat.setForeground(QBrush(QColor(m_fgWorkDay)));
    m_workdayFormat.setBackground(QBrush(QColor(m_bgWorkDay)));
    m_dayOffFormat.setForeground(QBrush(QColor(m_fgDayOff)));
    m_dayOffFormat.setBackground(QBrush(QColor(m_bgDayOff)));
}

CalendarManager::~CalendarManager()
{

}

void CalendarManager::setDates(const DateList &_selectedDates)
{
    m_dates = _selectedDates;
    refreshPage();
}

void CalendarManager::dateClicked(const QDate &_date)
{
    int dayOfWeek;
    dayOfWeek = _date.dayOfWeek();
    if (dayOfWeek < Qt::Saturday)
    {
        if (m_dates.contains(_date))  {
            m_dates.removeAll(_date);
        }
        else {
            m_dates.append(_date);
        }
        formatDate(_date);
    }
    setSelectionStyle();
}

void CalendarManager::refreshPage()
{
    QDate   _date = QDate(yearShown(),monthShown(),1);
    QDate   _lastDayOfMonth = _date.addMonths(1);
    while ( _date < _lastDayOfMonth)
    {
        formatDate(_date);
        _date = _date.addDays(1);
    }
    setSelectionStyle();
}

void CalendarManager::formatDate(const QDate &_date)
{
    if (m_dates.contains(_date))  {
        setDateTextFormat(_date, m_dayOffFormat);
    }
    else {
        setDateTextFormat(_date, m_workdayFormat);
    }
    updateCell(_date);
}

void CalendarManager::setSelectionStyle()
{
    QString     selectionStyle("selection-color:%1; selection-background-color: %2;");
    QTableView *view = findChild<QTableView*>("qt_calendar_calendarview");
    if (view)
    {
        if (m_dates.contains(selectedDate()))
        {
            view->setStyleSheet(selectionStyle.arg(m_fgDayOff).arg(m_bgDayOff));
        }
        else
        {
            view->setStyleSheet(selectionStyle.arg(m_fgWorkDay).arg(m_bgWorkDay));
        }
    }
}
