#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QDate>
#include <QTime>
#include <QMenu>
#include <QAction>
#include <QCursor>
#include <QStringList>

#include <QMainWindow>

#include "CommonTypes.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void updateDisplay();

private slots:
    void on_actionSettings_triggered();

    void on_actionQuit_triggered();

private:
    enum EOpModes {E_App, E_Kiosk, E_Debug};

    Ui::MainWindow *ui;
    EOpModes        m_appMode;
    QDate           m_retireDate;
    QTime           m_retireTime;
    DateList        m_officeDays;
    DateList        m_daysOff;
    QAction*        m_pSettingsAct;
    QAction*        m_pQuitAct;
    QCursor         m_blankCursor; //for kiosk mode

    const int       SEC_PER_DAY = 24 * 60 * 60;
    const int       SEC_PER_HOUR = 60 * 60;
    const int       SEC_PER_MIN = 60;

    void    quitApp();
    void    configureApp();
    void    updateTimeRemaining();
    void    countDays();
    void    kioskHideCursor();
    void    kioskShowCursor();
};

#endif // MAINWINDOW_H
