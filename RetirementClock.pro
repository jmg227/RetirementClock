#-------------------------------------------------
#
# Project created by QtCreator 2018-02-16T05:18:56
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = RetirementClock
TEMPLATE = app

target.path=/home/pi/bin
INSTALLS+=target

# Create platform specific definitions
CONFIG(Desktop,Desktop|RPi3):DEFINES += PLATFORM_DESKTOP
CONFIG(RPi3,Desktop|RPi3):DEFINES += PLATFORM_RPI3

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
    CalendarManager.cpp \
    DlgSettings.cpp \
        main.cpp \
        MainWindow.cpp

HEADERS += \
    CalendarManager.h \
    CommonTypes.h \
    DlgSettings.h \
        MainWindow.h

FORMS += \
    DlgSettings.ui \
        MainWindow.ui

RESOURCES += \
    clock.qrc

win32:RC_ICONS += images/app-icon.ico
