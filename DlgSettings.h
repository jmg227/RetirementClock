#ifndef DLGSETTINGS_H
#define DLGSETTINGS_H

#include <QDialog>
#include <QDate>
#include <QTime>
#include "CommonTypes.h"

namespace Ui {
class DlgSettings;
}

class DlgSettings : public QDialog
{
    Q_OBJECT

public:
    explicit DlgSettings(QWidget *parent = nullptr);
    ~DlgSettings();
    QDate     retireDate() const;
    QTime     retireTime() const;
    DateList  officeDays() const;
    void      setRetireDateTime(const QDate& _retireDate, const QTime& _retireTime);
    void      setOfficeDays(const DateList& _officeDays);
    void      setDaysOff(const DateList& _daysOff);
    DateList  daysOff() const;

private slots:
    void on_calDaysOff_clicked(const QDate &date);

    void on_calDaysOff_currentPageChanged(int year, int month);

    void on_calOfficeDays_clicked(const QDate &date);

    void on_calOfficeDays_currentPageChanged(int year, int month);

private:
    Ui::DlgSettings *ui;

    const int m_initialTab = 0;

};

#endif // DLGSETDATE_H
