#ifndef CALENDARMANAGER_H
#define CALENDARMANAGER_H

#include <QObject>
#include <QWidget>

#include <QCalendarWidget>

#include <QStringList>
#include <QBrush>
#include <QColor>
#include <QFile>
#include <QList>
#include <QDate>
#include <QPen>
#include <QTextCharFormat>

#include "CommonTypes.h"

class CalendarManager : public QCalendarWidget
{
    Q_OBJECT

public:
    CalendarManager(QWidget *parent = nullptr);
    ~CalendarManager();

    void        setDates(const DateList& _daysOff);
    DateList    getDates() const { return m_dates; }
    void        dateClicked(const QDate& _date);
    void        refreshPage();

private:
    void        formatDate(const QDate& _date);
    void        setSelectionStyle();
    DateList m_dates;

    QTextCharFormat     m_workdayFormat;
    QTextCharFormat     m_dayOffFormat;
    static const QString       m_bgDayOff;
    static const QString       m_fgDayOff;
    static const QString       m_bgWorkDay;
    static const QString       m_fgWorkDay;
};

#endif // CALENDARMANAGER_H
