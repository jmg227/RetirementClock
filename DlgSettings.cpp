#include "DlgSettings.h"
#include "ui_DlgSettings.h"
#include <QDate>
#include <QTime>
#include "CommonTypes.h"

DlgSettings::DlgSettings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DlgSettings)
{
    ui->setupUi(this);
    ui->tabWidget->setCurrentIndex(m_initialTab);
    ui->calDaysOff->refreshPage();
}

DlgSettings::~DlgSettings()
{
    delete ui;
}

QDate DlgSettings::retireDate() const
{
    return ui->retireDate->selectedDate();
}

QTime DlgSettings::retireTime() const
{
    return ui->retireTime->time();
}

DateList DlgSettings::officeDays() const
{
    return ui->calOfficeDays->getDates();
}

void DlgSettings::setRetireDateTime(const QDate &_retireDate, const QTime &_retireTime)
{
    ui->retireDate->setSelectedDate(_retireDate);
    ui->retireTime->setTime(_retireTime);
}

void DlgSettings::setOfficeDays(const DateList &_officeDays)
{
    ui->calOfficeDays->setDates(_officeDays);
}

void DlgSettings::setDaysOff(const DateList &_daysOff)
{
    ui->calDaysOff->setDates(_daysOff);
}

DateList DlgSettings::daysOff() const
{
    return ui->calDaysOff->getDates();
}

void DlgSettings::on_calDaysOff_clicked(const QDate &date)
{
    ui->calDaysOff->dateClicked(date);
}

void DlgSettings::on_calDaysOff_currentPageChanged(int year, int month)
{
    Q_UNUSED(year)
    Q_UNUSED(month)

    ui->calDaysOff->refreshPage();
}

void DlgSettings::on_calOfficeDays_clicked(const QDate &date)
{
    ui->calOfficeDays->dateClicked(date);
}

void DlgSettings::on_calOfficeDays_currentPageChanged(int year, int month)
{
    Q_UNUSED(year)
    Q_UNUSED(month)

    ui->calOfficeDays->refreshPage();
}
