#include <QApplication>
#include <QDebug>
#include <QTimer>
#include <QString>
#include <QSettings>
#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QMessageBox>
#include <QDate>
#include <QTime>
#include <QMenu>
#include <QFont>
#include <QAction>
#include <QIcon>
#include <QProcessEnvironment>
#include <QGuiApplication>
#include <QVariant>

#include "CommonTypes.h"
#include "MainWindow.h"
#include "ui_MainWindow.h"
#include "DlgSettings.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_retireDate(int(2020), int(1), int(17)),
    m_retireTime(int(17), int(0), int(0)),
    m_pSettingsAct(nullptr), m_pQuitAct(nullptr),
    m_blankCursor(Qt::BlankCursor)
{
    qRegisterMetaTypeStreamOperators<DateList>("DateList");

    // record the operating mode for the app
    QString appMode;
    appMode = QProcessEnvironment::systemEnvironment().value("RETIREMENTCLOCK_MODE", "app").toLower();
    if (appMode == QString("kiosk")) {
        m_appMode = E_Kiosk;
    }
    else if (appMode == QString("debug")) {
        m_appMode = E_Debug;
    }
    else {
        m_appMode = E_App;
    }

    QSettings   settings("JimSoft", "RetirementClock");
    QDate       defaultRetireDate(2020, 3, 5);
    QTime       defaultRetireTime(17,0,0);
    QDate       defaultMoveToCambridge(2020, 1, 18);

    ui->setupUi(this);

    // create a spacer widget so that the quit button will be on the right
    QWidget* spacer = new QWidget();
    spacer->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    ui->toolBar->insertWidget(ui->actionQuit,spacer);

    // find the application configuration file and load it.  Search order:
    // ${HOME}/.retirementClock/config.qss
    // ./config.qss
    // :default.qss
    QFileInfoList   configFileList;
    configFileList.append(QFileInfo(QDir::home(),QString(".retirementClock/config.qss")));
    configFileList.append(QFileInfo(QDir::current(), QString("config.qss")));
    configFileList.append(QFileInfo(QString(":default.qss")));

    foreach (QFileInfo cfgFile, configFileList)
    {
        qDebug() << "Checking for config file \"" << cfgFile.filePath() << "\"\n";
        if (cfgFile.exists())
        {
            qDebug() << "Reading configuration from \"" << cfgFile.filePath() << "\"\n";
            QFile   f(cfgFile.filePath());
            f.open(QFile::ReadOnly | QFile::Text);
            QTextStream ts(&f);
            QString styleSheet = ts.readAll();
            setStyleSheet(styleSheet);
            break;
        }
    }

    m_retireDate = settings.value("RetireDate", QVariant(defaultRetireDate)).toDate();
    m_retireTime = settings.value("RetireTime", QVariant(defaultRetireTime)).toTime();
    m_officeDays = settings.value("OfficeDays").value<DateList>();
    m_daysOff = settings.value("DaysOff").value<DateList>();

    QTimer* pStartTimer = new QTimer(this);
    pStartTimer->setSingleShot(true);
    connect(pStartTimer, SIGNAL(timeout()), this, SLOT(updateDisplay()));
    pStartTimer->start(250);
    QTimer* pTimer = new QTimer(this);
    connect(pTimer, SIGNAL(timeout()), this, SLOT(updateDisplay()));
    pTimer->start(30000);

    kioskHideCursor();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::updateDisplay()
{
    updateTimeRemaining();
    countDays();
}

void MainWindow::on_actionSettings_triggered()
{
    configureApp();
}

void MainWindow::on_actionQuit_triggered()
{
    quitApp();
}

void MainWindow::quitApp()
{
    switch (m_appMode) {
    case E_Kiosk:
        system("sudo systemctl halt");
        break;
    // fill in debug mode later if needed.
    default:
        QApplication::exit();
        break;
    }
}

void MainWindow::configureApp()
{
    DlgSettings  dlgSettings;
    dlgSettings.setRetireDateTime(m_retireDate, m_retireTime);
    dlgSettings.setDaysOff(m_daysOff);
    dlgSettings.setOfficeDays(m_officeDays);

    kioskHideCursor();
    if (dlgSettings.exec() == QDialog::Accepted)
    {
        QSettings   settings("JimSoft", "RetirementClock");
        m_retireDate = dlgSettings.retireDate();
        m_retireTime = dlgSettings.retireTime();
        m_officeDays = dlgSettings.officeDays();
        m_daysOff =  dlgSettings.daysOff();
        settings.setValue("RetireDate", QVariant(m_retireDate));
        settings.setValue("RetireTime", QVariant(m_retireTime));
        settings.setValue("OfficeDays", QVariant::fromValue(m_officeDays));
        settings.setValue("DaysOff",QVariant::fromValue(m_daysOff));
        updateDisplay();
    }
    kioskShowCursor();
}

void MainWindow::updateTimeRemaining()
{
    qint64  secLeft;
    int daysLeft;
    int hrsLeft;
    int minLeft;
    //QDateTime   now = QDateTime::currentDateTime();
    QDate   today = QDate::currentDate();
    QTime   now = QTime::currentTime();
    //qDebug() << "It is now " << now.toString() << endl;

    //secLeft = now.secsTo(m_retireDateTime);
    secLeft = (today.daysTo(m_retireDate) * 24 * 3600) + now.secsTo(m_retireTime);
    daysLeft = int(secLeft/SEC_PER_DAY);
    ui->daysLeft->setText(QString("%1").arg(daysLeft));
    secLeft = secLeft % SEC_PER_DAY;
    hrsLeft = int(secLeft/SEC_PER_HOUR);
    ui->hrsLeft->setText(QString("%1").arg(hrsLeft));
    secLeft = secLeft % SEC_PER_HOUR;
    minLeft = int(secLeft/SEC_PER_MIN);
    ui->minLeft->setText(QString("%1").arg(minLeft));
}

void MainWindow::countDays()
{
    QDate       today;
    QDate       testDate;
    QTime       now;
    int         numWorkDays = 0;
    int         numEventDays = 0;


    now = QTime::currentTime();
    // check to see if we count today
    for (testDate = today = QDate::currentDate(); testDate <= m_retireDate; testDate = testDate.addDays(1))
    {
        int   dayOfWeek = testDate.dayOfWeek();
        //is this a workday?
        //first, check if it is a weekend day or a day off
        if ((dayOfWeek != Qt::Sunday) && (dayOfWeek != Qt::Saturday) && !m_daysOff.contains(testDate))
        {
            // only count if it is not today or if today is before noon
            if ((now.hour() < 12) || (testDate != today))
            {
                numWorkDays++;
                // if it's a workday, see if we're in the office
                if (m_officeDays.contains(testDate))
                {
                    numEventDays++;
                }
            }
        }
    }

    if (numWorkDays > ui->workDaysLeft->maximum())
    {
        ui->workDaysLeft->setMaximum(numWorkDays);
    }
    ui->workDaysLeft->setValue(numWorkDays);
    if (numEventDays > ui->eventDaysLeft->maximum())
    {
        ui->eventDaysLeft->setMaximum(numEventDays);
    }
    ui->eventDaysLeft->setValue(numEventDays);
}

void MainWindow::kioskHideCursor()
{
    // If in kiosk mode, hide the cursor
    if (E_Kiosk == m_appMode)
    {
        QGuiApplication::setOverrideCursor(m_blankCursor);
    }
}

void MainWindow::kioskShowCursor()
{
    // If in kiosk mode, hide the cursor
    if (E_Kiosk == m_appMode)
    {
        QGuiApplication::restoreOverrideCursor();
    }
}
